import numpy as np
import cv2
import sklearn.metrics
import matplotlib.pyplot as plt
from time import time
import phasepack
import concurrent.futures
import sys


num_candidates = 3
Lowe = 0.99
stamp_size = 800


def phase_congruenc_of_image(image, nscale, minWaveLnegth):
    canny, _, _, _ = phasepack.phasecongmono(image, nscale=nscale, minWaveLength=minWaveLnegth)
    canny = canny.astype(np.float32)
    return canny


def full_edge_detection(image):
    num_grid_boxes = 5
    (h, w) = image.shape
    h = int(h / num_grid_boxes)
    w = int(w / num_grid_boxes)
    canny_image = np.zeros(image.shape, dtype=np.uint8)
    for i in range(num_grid_boxes):
        for j in range(num_grid_boxes):
            sub_image = image[i * h:(i + 1) * h, j * w:(j + 1) * w]
            temp = partial_edge_detection(sub_image)
            canny_image[i * h:(i + 1) * h, j * w:(j + 1) * w] = temp
    return canny_image


def partial_edge_detection(image):
    x_tag = np.diff(image, axis=0)
    y_tag = np.diff(image, axis=1)
    tag_tag = np.concatenate([x_tag.ravel(), y_tag.ravel()], axis=0)
    min_val = np.max([30, np.mean(tag_tag) - 0.5 * np.std(tag_tag)])
    max_val = np.min([180, np.mean(tag_tag) + 1.5 * np.std(tag_tag)])
    canny_image = cv2.Canny(image, min_val, max_val)
    if np.sum(canny_image) == 0:  # If no edges, object must be very big
        canny_image = cv2.Canny(image, 20, 70)
    return canny_image


def find_candidate(scale, stamp, canny_viz, canny_swir, match_results, num_thread):
    match_threshold = 0.01
    stamp_buffer = 80  # in pixels
    (h, w) = stamp.shape
    kk = num_thread * num_candidates
    if w * scale < canny_viz.shape[1] and h * scale < canny_viz.shape[0]:
        stamp_scaled = cv2.resize(stamp, dsize=(int(w * scale), int(h * scale)), interpolation=cv2.INTER_AREA)
        # canny_stamp_scaled = full_edge_detection(stamp_scaled)
        canny_stamp_scaled = cv2.resize(canny_swir, dsize=(int(w * scale), int(h * scale)), interpolation=cv2.INTER_AREA)
        if np.sum(canny_stamp_scaled) == 0:  # If no edges, object must be very big
            canny_stamp_scaled = stamp_scaled
        candidate = num_candidates
        match = cv2.matchTemplate(canny_viz, canny_stamp_scaled, cv2.TM_CCORR_NORMED)
        while candidate > 0:
            (_, max_val, _, max_loc) = cv2.minMaxLoc(match)
            if max_val > match_threshold:
                match_results[kk][0] = max_val
                match_results[kk][1] = max_loc[0]
                match_results[kk][2] = max_loc[1]
                match_results[kk][3] = scale
                kk = kk + 1

                min_row = np.max([0, max_loc[1] - stamp_buffer])
                max_row = np.min([canny_viz.shape[1], max_loc[1] + stamp_buffer])
                min_col = np.max([0, max_loc[0] - stamp_buffer])
                max_col = np.min([canny_viz.shape[0], max_loc[0] + stamp_buffer])
                match[min_row:max_row, min_col:max_col] = 0
            candidate = candidate - 1
    return


def find_best_mutual_information(gray_viz, stamp, match_results, candidate_refinement):
    (h, w) = stamp.shape
    for i in range(len(match_results)):
        if match_results[i][0] != 0:
            cur_scale = match_results[i][3]
            stamp_scaled = cv2.resize(stamp, dsize=(int(w * cur_scale), int(h * cur_scale)))
            min_row = int(match_results[i][2])
            max_row = int(np.min([gray_viz.shape[0], match_results[i][2] + cur_scale * h]))
            min_col = int(match_results[i][1])
            max_col = int(np.min([gray_viz.shape[1], match_results[i][1] + cur_scale * w]))
            viz_area = gray_viz[min_row:max_row, min_col:max_col].copy()
            viz_area = cv2.resize(viz_area, (stamp_scaled.shape[1], stamp_scaled.shape[0]))
            mi = sklearn.metrics.mutual_info_score(stamp_scaled.ravel(), viz_area.ravel())
            candidate_refinement[i] = mi
    return


def find_matches_in_candidates(match_results, gray_viz, stamp, features, queryKeypoints, queryDescriptors, candidate_refinement):
    (h, w) = stamp.shape
    matcher = cv2.BFMatcher()
    for i in range(len(match_results)):
        if match_results[i][0] != 0:
            cur_scale = match_results[i][3]
            min_row = int(match_results[i][2])
            max_row = int(np.min([gray_viz.shape[0], match_results[i][2] + cur_scale * h]))
            min_col = int(match_results[i][1])
            max_col = int(np.min([gray_viz.shape[1], match_results[i][1] + cur_scale * w]))
            viz_area = gray_viz[min_row:max_row, min_col:max_col].copy()
            trainKeypoints, trainDescriptors = features.detectAndCompute(viz_area, None)
            # Apply ratio test
            good_matches = []
            good_query_pts = []
            good_training_pts = []
            if (queryDescriptors is not None) and (trainDescriptors is not None):
                matches = matcher.knnMatch(queryDescriptors, trainDescriptors, k=2)
                for m_n in matches:
                    if len(m_n) == 2:
                        (m, n) = m_n
                        if m.distance < Lowe * n.distance:
                            good_matches.append([m])
                            good_query_pts.append(queryKeypoints[m.queryIdx].pt)
                            good_training_pts.append(trainKeypoints[m.trainIdx].pt)

            if len(good_training_pts) > 0:
                good_query_pts = np.float32(good_query_pts)
                good_training_pts = np.float32(good_training_pts)
                H, status = cv2.findHomography(good_query_pts, good_training_pts, cv2.RANSAC, 3.0)
                candidate_refinement[i] = np.sum(status) / (h * w * cur_scale * cur_scale)
            else:
                candidate_refinement[i] = 0


def main(image_swir, image_viz, point):

    gray_viz = cv2.cvtColor(image_viz, cv2.COLOR_BGR2GRAY)
    gray_swir = cv2.cvtColor(image_swir, cv2.COLOR_BGR2GRAY)

    # Select Stamp
    # ROI = cv2.selectROI(gray_swir)
    # stamp = gray_swir[ROI[1]:ROI[1] + ROI[3], ROI[0]:ROI[0] + ROI[2]].copy()
    min_row = int(np.max([0, point[0] - stamp_size / 2]))
    max_row = int(np.min([gray_swir.shape[0], point[0] + stamp_size / 2]))
    min_col = int(np.max([0, point[1] - stamp_size / 2]))
    max_col = int(np.min([gray_swir.shape[1], point[1] + stamp_size / 2]))
    stamp = gray_swir[min_row:max_row, min_col:max_col].copy()

    # Start counting time
    start_time = time()
    # Canny transformation
    # canny_viz = full_edge_detection(gray_viz)
    with concurrent.futures.ThreadPoolExecutor() as executor:
        future = executor.submit(phase_congruenc_of_image, gray_viz, 4, 5)
        future2 = executor.submit(phase_congruenc_of_image, stamp, 4, 3)
        canny_viz = future.result()
        canny_swir = future2.result()

    # Generate candidates per scale
    scales = [0.25, 0.5, 0.75, 1.0, 1.25]
    match_results = np.zeros([len(scales) * num_candidates, 4])  # max_val, x, y, scale

    for scale_ind in range(len(scales)):
        cur_scale = scales[scale_ind]
        find_candidate(cur_scale, stamp, canny_viz, canny_swir, match_results, scale_ind)

    # Find best candidate
    method = "MI"
    candidate_refinement = np.zeros(len(match_results))
    patch_size = 17
    num_features = 1200
    if method == "MI":
        find_best_mutual_information(gray_viz, stamp, match_results, candidate_refinement)
    else:
        if method == "ORB":
            features = cv2.ORB_create(nfeatures=num_features, scaleFactor=1.15, nlevels=10, edgeThreshold=patch_size, patchSize=patch_size)
        else:
            features = cv2.SIFT_create(nfeatures=num_features, nOctaveLayers=4, edgeThreshold=25, contrastThreshold=0.01, sigma=1.0)
        queryKeypoints, queryDescriptors = features.detectAndCompute(stamp, None)
        find_matches_in_candidates(match_results, gray_viz, stamp, features, queryKeypoints, queryDescriptors, candidate_refinement)

    ind = np.argmax(candidate_refinement)
    sub_candidate_refinement = candidate_refinement.copy()
    sub_candidate_refinement[ind] = 0
    ind2 = np.argmax(sub_candidate_refinement)

    if (ind2 == ind) or (candidate_refinement[ind2] == 0):
        success = False
    else:
        if candidate_refinement[ind] > candidate_refinement[ind2] / Lowe:
            success = True
        else:
            success = False

    # Count candidates
    found_candidates = 0
    for i in range(len(match_results)):
        if match_results[i][0] != 0:
            found_candidates = found_candidates + 1

    # Print candidates on image
    (h, w) = stamp.shape
    print_frame = image_viz.copy()
    for i in range(len(match_results)):
        if match_results[i][0] != 0:
            pt1 = (int(match_results[i][1]), int(match_results[i][2]))
            pt2 = (int(match_results[i][1] + w * match_results[i][3]), int(match_results[i][2] + h * match_results[i][3]))
            # if i == ind:
            cv2.rectangle(print_frame, pt1, pt2, (0, 0, 0), 3)

    # Print best candidate
    if success:
        pt = (int(match_results[ind][1] + w * match_results[ind][3] / 2), int(match_results[ind][2] + h * match_results[ind][3] / 2))
        print_frame = cv2.circle(print_frame, pt, 5, (0, 0, 255), -1)

    # End time counting
    end_time = time()

    # Prints
    print("Found", found_candidates, "candidates!")
    print("Best candidate has a score of", candidate_refinement[ind])
    print("Calculations took", end_time - start_time, "seconds")


# main function - argymenr list is swir_image, viz_image, p_x, p_y
if __name__ == "__main__":
    image_swir = sys.argv[1]  # currently is string, need to make it an opencv matrix
    image_viz = sys.argv[2]  # currently is string, need to make it an opencv matrix
    p_x = int(sys.argv[3])
    p_y = int(sys.argv[4])
    point = (p_y, p_x)
    main(image_swir, image_viz, point)
